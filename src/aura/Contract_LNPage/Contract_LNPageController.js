({
    doinit : function(component, event, helper) {
        
        var action = component.get("c.fetchContract");
        
        if (component.get("v.recordId") == undefined){
            console.log('first if');
        } else {
            console.log('else');
            action.setParams({idd : component.get("v.recordId")});
            action.setCallback(this, function(a) {
                component.set("v.conObj", a.getReturnValue());
            });
            $A.enqueueAction(action);
        }
        
        component.set('v.ctccolumns', [
            {label: 'Contract to Clause', fieldName: 'Name', type: 'text'}//,
            //{label: 'Contract', fieldName: 'ContractNumber', type: 'text'},
            //{label: 'Clause', fieldName: 'ClauseName', type: 'text'} 
        ]);
        
        helper.getctcData(component);
        
        component.set('v.columns', [
            {label: 'Clause Name', fieldName: 'Name', type: 'text'},
            {label: 'Clause Type', fieldName: 'Type__c', type: 'text'}
        ]);
        
        helper.getData(component);
    },
    showModal : function (component, event, helper) {
        console.log('boop');
        document.getElementById(component.get('v.recordId') + "modalbox").style.display = "block";
    },
    
    hideModal : function (component, event, helper) {
        console.log('boop');
        document.getElementById(component.get('v.recordId') + "modalbox").style.display = "none";
    },
    
    updateSelectedText: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        cmp.set('v.selectedRowsCount', selectedRows.length);
    },
    
    ctcupdateSelectedText: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        cmp.set('v.ctcselectedRowsCount', selectedRows.length);
    },
    
    addSelectedClauses: function (component, event, helper) {
        var action = component.get('c.createCtcs');
        var dTable = component.find("clauseTable");
        var rows = dTable.getSelectedRows();
        if (typeof rows != 'undefined' && rows.length>0) {
            var clauseIds = [];
            for(var i=0;i<rows.length;i++){
                clauseIds.push(rows[i].Id);
            }         
            action.setParams({
                "idd" : component.get('v.recordId'),
                "conIds" : clauseIds
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state==="SUCCESS") {
                    var event = $A.get('e.force:navigateToSObject');
                    event.setParams({
                        "recordId": component.get('v.recordId')
                    });
                    
                    event.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    deleteSelectedCtcs: function (component, event, helper) {
        var action = component.get('c.deleteCtcs');
        var dTable = component.find("ctcTable");
        var rows = dTable.getSelectedRows();
        if (typeof rows != 'undefined' && rows.length>0) {
            var ctcIds = [];
            for(var i=0;i<rows.length;i++){
                ctcIds.push(rows[i].Id);
            }
            action.setParams({
                "ctcIds" : ctcIds
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state ==="SUCCESS") {
                    var event = $A.get('e.force:navigateToSObject');
                    event.setParams({
                        "recordId": component.get('v.recordId')
                    });
                    event.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    searchKeyChange: function (component, event, helper) {
        var myEvent = $A.get("e.c:searchkeychange");

        myEvent.setParams({"searchkey1": event.target.value});

        myEvent.fire();        
    },
    
    searchKeyChange2: function (component, event, helper) {
        var searchKey = event.getParam("searchkey1");
        var action = component.get("c.searchClause");
        
        action.setParams({
            "searchkey": searchKey
        });
        action.setCallback(this, function(a) {
            component.set("v.data", a.getReturnValue());
        });
        
        $A.enqueueAction(action);
    },
})