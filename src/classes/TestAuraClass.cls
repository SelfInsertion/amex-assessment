@isTest
public class TestAuraClass {
    
    @testSetup
    static void setupData () {
        Account acc = new Account(Name = 'Iain Henderson');
        insert acc;
        Contract con = new Contract(AccountId = acc.Id);
        Contract_Clause__c clause = new Contract_Clause__c(Name = 'Clause One');
        
        insert con;
        insert clause;
        
        Contract_To_Clause__c ctc = new Contract_To_Clause__c(Contract__c=con.Id, Contract_Clause__c = clause.Id);
        insert ctc;
    }
    
    @isTest
    static void testAllMethods () {
        //AuraClass a = new AuraClass();
        Test.startTest();
        
        List<Contract> conList = [SELECT Name, Id FROM Contract];
        Contract fetchTest = AuraClass.fetchContract(conList[0].Id);
        System.assert(fetchTest != null);
        
        List<Contract_Clause__c> clauseList = AuraClass.getAllClauses();
        System.assert(clauseList == (List<Contract_Clause__c>)[SELECT Name, Id, Type__c FROM Contract_Clause__c]);
        
        List<Contract_To_Clause__c> ctcList = [SELECT Name, Id FROM Contract_To_Clause__c];
        List<Contract_To_Clause__c> getCtcsTest = AuraClass.getCtcs(conList[0].Id);
        System.assert(getCtcsTest[0].Id == ctcList[0].Id);
        
        List<String> idList = new List<String>();
        for (Contract_Clause__c cc : clauseList){
            idList.add((String)cc.Id);
        }
        
        AuraClass.createCtcs(conList[0].Id, idList);
        List<String> fakeIds = new List<String>();
        fakeIds.add('meatballs');
        fakeIds.add('parmesan');
        AuraClass.createCtcs('a01f2000011n3TpAAI', fakeIds);
        AuraClass.deleteCtcs(idList);
        
        AuraClass.searchClause('first');
        
        Test.stopTest();
    }
}