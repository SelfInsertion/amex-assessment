public with sharing class AuraClass {
    
    //public void AuraClass() {}
    public static Contract con;
    
    @AuraEnabled
    public static Contract fetchContract (Id idd) {
        con = [SELECT Id, Name FROM Contract WHERE id=:idd];
        return con;
    }
    
    @AuraEnabled
    public static List<Contract_Clause__c> getAllClauses () {
        List<Contract_Clause__c> clauses = [SELECT Name, Id, Type__c FROM Contract_Clause__c];
        return clauses;
    }
    
    @AuraEnabled
    public static List<Contract_to_Clause__c> getCtcs (Id idd) {
        List<Contract_to_Clause__c> ctcs = [SELECT Name, Id, Contract__r.ContractNumber , Contract_Clause__r.Name FROM Contract_to_Clause__c WHERE Contract__c=:idd];
        system.debug(ctcs);
        return ctcs;
    }
    
    @AuraEnabled
    public static void createCtcs (Id idd, List<String> conIds) {
        try {
            if (conIds.size() > 0){
                List<Contract_To_Clause__c> ctcs = new List<Contract_To_Clause__c>();
                Contract con = [SELECT Name, Id FROM Contract WHERE Id=:idd];
                List<Contract_Clause__c> ccs = [SELECT Name, Id FROM Contract_Clause__c WHERE Id=:conIds];
                
                for (string conId : conIds){
                    Contract_To_Clause__c ctc = new Contract_To_Clause__c();
                    String concla = '';
                    for (Contract_Clause__c cc : ccs){
                        if (cc.Id == conId){
                            concla = cc.Name;
                        }
                    }
                    ctc.Contract_Clause__c = conId;
                    ctc.Contract__c = idd;
                    ctc.Name = con.Id + ': ' + conCla;
                    ctcs.add(ctc);
                }
                if (ctcs.size()>0){
                    insert ctcs;
                }
            }
        }
        catch (Exception e) {
            system.debug(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static void deleteCtcs (List<String> ctcIds) {
        List<Contract_To_Clause__c> ctcs = new List<Contract_To_Clause__c>();
        ctcs = [SELECT Id FROM Contract_To_Clause__c WHERE Id IN :ctcIds];
        delete ctcs;
    }
    
    @AuraEnabled
    public static List<Contract_Clause__c> searchClause(String searchkey) {
        String name = '%' + searchkey + '%';
        return [SELECT Id, Name, Type__c FROM Contract_Clause__c WHERE (Name LIKE :name) OR (Type__c LIKE :name)];
    }
}