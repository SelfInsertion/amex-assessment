({
    getData : function(cmp) {
        var action = cmp.get('c.getAllClauses');
        action.setCallback(this, $A.getCallback(function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                cmp.set('v.data', response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    },
    
    getctcData : function(cmp) {
        var action = cmp.get('c.getCtcs');
        action.setParams({idd : cmp.get("v.recordId")});
        action.setCallback(this, $A.getCallback(function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var rows = response.getReturnValue();
                console.log("LOOK HERE: "+rows);
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    console.log("AND HERE: "+row);
                    if (row.Contract__c) row.ContractNumber = row.Contract__c.ContractNumber;
                    if (row.Contract_Clause__c) row.ClauseName = row.Contract_Clause__c.ContractNumber;
                }
                cmp.set('v.ctcdata', rows);
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    }
})